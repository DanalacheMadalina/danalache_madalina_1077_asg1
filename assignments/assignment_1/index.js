
const FIRST_NAME = "Madalina";
const LAST_NAME = "Danalache";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(input) {
	if (input == Number.POSITIVE_INFINITy || input == Number.NEGATIVE_INFINIT) return NaN;
    else if (input > Number.MAX_SAFE_INTEGER || input < Number.MIN_SAFE_INTEGER) return NaN;	
    else if(isNaN(input)==true) return NaN;
    else if (typeof(input) == "string" ) return parseInt(input);
    else return parseInt(input);
  
	
}
module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

